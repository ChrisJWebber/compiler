﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Compiler
{

    class CompilationEngine :IDisposable
    {
        bool disposed = false;
        bool tokenConsumed = true;
        bool identifierAsType = false;
        kind currentKind = kind.NONE;
        string currentType = "";
        string currentClass = "";
        string savedToken = "";
        string subroutineType = "";
        
        int subroutineArgs = 0;
        int labelId = 0;

        JackTokeniser Tokeniser;
        VMWriter Writer;
        SymbolTable symbolTable;
        

        public CompilationEngine (string inputFilePath, string outputFilePath )
        {
            this.Tokeniser = new JackTokeniser(inputFilePath);
            this.Writer = new VMWriter(outputFilePath);
            this.symbolTable = new SymbolTable();

            while (Tokeniser.hasMoreTokens())
            {
                advanceTokeniser();

                if (Tokeniser.tokenType() == token.KEYWORD)
                    if (Tokeniser.keyWord() == "class")
                        compileClass();
            }
                System.Console.WriteLine("End of file\n");
                Writer.close();
                Tokeniser.Dispose();
                Writer.Dispose();
        }

        #region privateMethods

        //Compiles a complete class
        private void compileClass()
        {
            //discard class keyword
            consumeToken();

            //class name
            advanceTokeniser();
            if (Tokeniser.tokenType() != token.IDENTIFIER)
                exitWithError("Identifier expected after Class keyword");
            currentClass = Tokeniser.indentifier();
            consumeToken();

            //Class open
            advanceTokeniser();
            checkForSymbol('{', true);
            consumeToken();

            //Vars and subroutines
            compileClassVarDec();
            compileSubroutineDec();

            //class close
            advanceTokeniser();
            checkForSymbol('}', true);
            consumeToken();
        }

        //Compiles a static variable or field declaration
        private void compileClassVarDec()
        {
            advanceTokeniser();
            while (checkForClassVar())
            {
                //consume kind
                consumeToken();

                // type declaration
                compileVarList(true, false);

                //end classVars
                advanceTokeniser();
            }
        }

        //Compiles a complete method, function, or constructor
        private void compileSubroutineDec()
        {
            string subroutineName = "";

            advanceTokeniser();
            while (checkForSubroutineDec())
            {
                int nLocals = 0;
                symbolTable.startSubroutine();

                //subroutine type
                subroutineType = Tokeniser.keyWord();
                consumeToken();

                //return type
                advanceTokeniser();
                checkForReturnType();
                consumeToken();

                //subroutineName
                advanceTokeniser();
                checkForSubroutineName();
                subroutineName = currentClass + "." + Tokeniser.indentifier();
                consumeToken();

                //Open parens
                advanceTokeniser();
                checkForSymbol('(',true);
                consumeToken();

                compileParameterList();

                //close params
                advanceTokeniser();
                checkForSymbol(')', true);
                consumeToken();

                //subroutine open
                advanceTokeniser();
                checkForSymbol('{', true);
                consumeToken();

                //Check for var dec
                advanceTokeniser();
                while (checkForVar())
                {
                    nLocals += compileVarDec();
                    advanceTokeniser();
                }
                    

                writeFunction(subroutineName, nLocals);

                if (subroutineType == "constructor")
                {
                    int classSize = symbolTable.VarCount(kind.FIELD);

                    writePush(segment.constant, classSize, false);
                    Writer.writeCall("Memory.alloc", 1);
                    writePop(segment.pointer, 0, false);
                }

                compileSubroutineBody();
                advanceTokeniser();
            }
        }

        //Compiles a (possibly empty) parameter list. Does not handle the enclosing (). 
        private void compileParameterList()
        {
            advanceTokeniser();
            if (checkForVarType(false))
            {
                currentKind = kind.ARG;
                //add class to symbol table as arg 0 when calling a method
                if (subroutineType == "method")
                    symbolTable.define("this", currentClass, currentKind);
                compileVarList(false, true);
            }
            currentType = "";
            currentKind = kind.NONE;
        }

        //Compiles a subroutines body
        private void compileSubroutineBody()
        {
            //Associate this segment with current object
            if (subroutineType == "method")
            {
                writePush(segment.argument, 0, false);
                writePop(segment.pointer, 0, false);
            }

            compileStatements();            

            //subroutine close
            advanceTokeniser();
            checkForSymbol('}', true);
            consumeToken();
        }

        //Compiles a var declaration
        private int compileVarDec()
        {
            //Write Var
            advanceTokeniser();
            consumeToken();

            return compileVarList(true, false);
        }

        //Compiles a sequence of statements. does not handle the enclosing {}
        private void compileStatements()
        {
            //check for statements
            advanceTokeniser();
            while (checkForStatements())
            {
                string key = Tokeniser.keyWord();
                switch (key)
                {
                    case "let":
                        compileLet();
                        break;

                    case "if":
                        compileIf();
                        break;

                    case "while":
                        compileWhile();
                        break;

                    case "do":
                        compileDo();
                        break;

                    case "return":
                        compileReturn();
                        break;

                }
                advanceTokeniser();
            }
        }

        //Compiles a let statement
        private void compileLet()
        {
            string varName = "";
            bool arrayAccess = false;

            //consume let keyword 
            consumeToken();

            //save var name
            advanceTokeniser();
            checkForVarName();
            varName = Tokeniser.indentifier();
            consumeToken();

            //write expression
            advanceTokeniser();
            if (checkForSymbol('[', false))
            {
                arrayAccess = true;
                // consume [
                consumeToken();
                advanceTokeniser();

                writeVar(varName, false);
                checkForExpression(true);
                compileExpression();

                //Add array base address and index reulst of expression
                writeArithmetic(command.ADD);

                //consume symbol
                advanceTokeniser();
                checkForSymbol(']', true);
                consumeToken();
            }

            //consume =
            advanceTokeniser();
            checkForSymbol('=', true);
            consumeToken();

            //write expression
            advanceTokeniser();
            checkForExpression(true);
            compileExpression();

            //consume ;
            advanceTokeniser();
            checkForSymbol(';', true);
            consumeToken();

            if (arrayAccess)
            {
                arrayAccess = false;

                //save expression into temp
                writePop(segment.temp, 0, false);

                //top of stack is now address of array element to be changed
                //pop address into "that" segment
                writePop(segment.pointer, 1, false);

                //put expression result into that
                writePush(segment.temp, 0, false);
                writePop(segment.that, 0, false);
            }
            else
            {
                //pop into var name
                if (symbolTable.IndexOf(varName) == -1)
                    exitWithError("Varibale not defined");
                writePop(kindToSegment(symbolTable.KindOf(varName)), symbolTable.IndexOf(varName), true);
            }
           
        }

        //Compiles an if statement possibly with trailing else clause
        private void compileIf()
        {
            int ifId = ++labelId;

            //consume if keyword 
            consumeToken();

            //consume (
            advanceTokeniser();
            checkForSymbol('(', true);
            consumeToken();

            //compile expression
            advanceTokeniser();
            checkForExpression(true);
            compileExpression();
            Writer.writeArithmetic(command.NOT);

            ifId++;
            labelId++;
            Writer.WriteIf("if" + ifId.ToString());

            //consume )
            advanceTokeniser();
            checkForSymbol(')', true);
            consumeToken();

            //consume {
            advanceTokeniser();
            checkForSymbol('{', true);
            consumeToken();

            compileStatements();
            ifId++;
            labelId++;
            Writer.WriteGoto("if" + ifId.ToString());
            Writer.WriteLabel("if" + (ifId - 1).ToString());

            //consume }
            advanceTokeniser();
            checkForSymbol('}', true);
            consumeToken();

            //compile else
            advanceTokeniser();
            if (checkForElse())
            {
                //consume else
                consumeToken();

                //consume {
                advanceTokeniser();
                checkForSymbol('{', true);
                consumeToken();

                compileStatements();

                //consume }
                advanceTokeniser();
                checkForSymbol('}', true);
                consumeToken();

            }
            Writer.WriteLabel("if" + ifId.ToString());

        }

        //Compiles a while statement
        private void compileWhile()
        {
            int whileId = ++labelId;

            //write while keyword 
            consumeToken();

            //consume (
            advanceTokeniser();
            checkForSymbol('(', true);
            consumeToken();

            //loop start
            Writer.WriteLabel("while" + whileId.ToString());

            //write expression
            advanceTokeniser();
            checkForExpression(true);
            compileExpression();
            Writer.writeArithmetic(command.NOT);

            whileId++;
            labelId++;
            Writer.WriteIf("while" + whileId.ToString());

            //consume )
            advanceTokeniser();
            checkForSymbol(')', true);
            consumeToken();

            //consume {
            advanceTokeniser();
            checkForSymbol('{', true);
            consumeToken();

            compileStatements();
            Writer.WriteGoto("while" + (whileId - 1).ToString());

            //consume }
            advanceTokeniser();
            checkForSymbol('}', true);
            writeToken();

            //exit
            Writer.WriteLabel("while" + whileId.ToString());
        }

        //Compiles a do statement
        private void compileDo()
        {

            //consume do keyword
            consumeToken();

            //subroutine call
            advanceTokeniser();
            compileSubroutineCall(false);

            //pop meaningless return value
            writePop(segment.temp, 0, true);

            //consume ;
            advanceTokeniser();
            checkForSymbol(';', true);
            consumeToken();

        }

        //Compiles a return statement
        private void compileReturn()
        {
            //consume return keyword
            consumeToken();

            //check for void return
            advanceTokeniser();
            if (checkForSymbol(';', false))
                 writePush(segment.constant, 0, false);
            else
            {
                //write expression
                advanceTokeniser();
                if (checkForExpression(false))
                    compileExpression();
            }

            //consume ;
            advanceTokeniser();
            checkForSymbol(';', true);
            consumeToken();

            //write return
            writeReturn();
        }

        //Compiles an expression 
        private void compileExpression()
        {
            //write term
            compileTerm();

            //check for more terms
            advanceTokeniser();
            while (checkForOp())
            {

                //save op
                char op = Tokeniser.symbol();
                consumeToken();

                //write term
                compileTerm();

                //write op
                writeSymbol(op);
                advanceTokeniser();
            }
        }

        /*Compiles a term. If the current token is an identifier
          the routine must distinguish between a variable, an array entry,
          or a subroutine call. A single look-ahead token, which
          may be one of [, (, ., suffices to distinguish
          between the possibilities. Any other token is not part
          of this term and should not be advanced over. 
         */
        private void compileTerm()
        {
            advanceTokeniser();
            token token = Tokeniser.tokenType();

            if (token == token.INT_CONST)
                writePush(segment.constant, Tokeniser.intVal(), true);

            else if (token == token.STRING_CONST)
                compileString();

            else if (checkForKeywordConst())
            {
                switch (Tokeniser.keyWord())
                {
                    case "true":
                        writePush(segment.constant, 0, true);
                        writeArithmetic(command.NOT);
                        break;

                    case "false":
                    case "null":
                        writePush(segment.constant, 0, true);
                        break;

                    case "this":
                        writePush(segment.pointer, 0, true);
                        break;
                }
            }
            else if (token == token.IDENTIFIER)
            {
                //save identifier
                saveToken();

                advanceTokeniser();
                if (checkForSymbol('[', false))
                {
                    //consume [
                    consumeToken();

                    //Add array index to base address 
                    writeVar(savedToken, false);
                    advanceTokeniser();
                    compileExpression();
                    writeArithmetic(command.ADD);
                    advanceTokeniser();
                    checkForSymbol(']', true);
                    consumeToken();

                    //push result of array index
                    writePop(segment.pointer, 1, false);
                    writePush(segment.that, 0, false);

                }
                else if ((checkForSymbol('(', false)) | (checkForSymbol('.', false))) //subroutine call
                {
                    currentKind = kind.SUBROUTINE;
                    compileSubroutineCall(true);
                }
                else
                {
                    writeVar(savedToken, false);
                    savedToken = "";
                }
            }
            else if (checkForSymbol('(', false))
            {
                consumeToken();
                advanceTokeniser();
                compileExpression();
                advanceTokeniser();
                checkForSymbol(')', true);
                consumeToken();
            }
            else if (checkForSymbol('-', false) | checkForSymbol('~', false))
            {
                command op = command.NEG;   //to silence a compiler error
                if (checkForSymbol('-', false))
                    op = command.NEG;
                if (checkForSymbol('~', false))
                    op = command.NOT;
                consumeToken();

                //write term
                advanceTokeniser();
                if (!checkForTerm())
                    exitWithError("Term expected");
                compileTerm();

                writeArithmetic(op);
            }

            else
                exitWithError("Term expected");
        }

        //Compiles a (possibly empty) comma-seperated list of expressions
        private void compileExpressionList()
        {

            if (checkForTerm())
            {
                subroutineArgs++;
                compileExpression();

                //Check for more expressions
                advanceTokeniser();
                while (checkForSymbol(',', false))
                {
                    subroutineArgs++;
                    consumeToken();
                    compileExpression();
                    advanceTokeniser();
                }

            }
        }

        //compiles a subroutine call
        private void compileSubroutineCall(bool writeSavedToken)
        {
            string name = "";
            subroutineArgs = 0;

            if (writeSavedToken)
            {
                if (checkDifferentClass(savedToken))
                {
                    //methods are called by variableName.methodName.
                    //Functionas and constructors are called by className.subroutineName
                    //Therefore if this evaluates to true it must be a method call hence push object reference
                    pushObjectRef(savedToken);
                    subroutineArgs++;
                    name += getClassName(savedToken);
                }

                else
                    name += savedToken;
                savedToken = "";
            }
            else
            {

                advanceTokeniser();
                checkForSubroutineName();
                if (checkDifferentClass(Tokeniser.indentifier()))        
                {
                    //methods are called by variableName.methodName.
                    //Functionas and constructors are called by className.subroutineName
                    //Therefore if this evaluates to true it must be a method call hence push object reference
                    pushObjectRef(Tokeniser.indentifier());
                    subroutineArgs++;
                    name += getClassName(Tokeniser.indentifier());
                }
                
                else
                    name += Tokeniser.indentifier();
                consumeToken();
            }

            //Check for class or var name
            advanceTokeniser();
            if (checkForSymbol('.',false))
            {
                //write .            
                name += '.';
                consumeToken();

                //subroutineName
                advanceTokeniser();
                checkForSubroutineName();
                name += Tokeniser.indentifier();
                consumeToken();
            }
            else
            {
                //subroutine must be in the current class. Prepend Class name to call
                //subroutine must be a method because of the missing class name.
                //push this object ref
                writePush(segment.pointer, 0, false);
                subroutineArgs++;
                name = currentClass + "." + name;
            }

            //consume (
            advanceTokeniser();
            checkForSymbol('(', true);
            consumeToken();

            //expression list
            advanceTokeniser();
            compileExpressionList();
            writeCall(name, subroutineArgs);

            //write )
            advanceTokeniser();
            checkForSymbol(')', true);
            consumeToken();

        }

        private void compileString()
        {
            string text = Tokeniser.stringVal();
            //create string of required length
            writePush(segment.constant, text.Length, false);
            Writer.writeCall("String.new", 1);

            foreach (char c in text)
            {
                writePush(segment.constant, (int)c, false);
                Writer.writeCall("String.appendChar", 2);
            }
            consumeToken();
        }

        private void pushObjectRef(string objectName)
        {
            //Push this as arg 0 to method call
            int index = symbolTable.IndexOf(objectName);
            if (index == -1)
                exitWithError("Unrecognised object");

            segment segment = kindToSegment(symbolTable.KindOf(objectName));
            writePush(segment, index, false);
        }

        private void advanceTokeniser()
        {
            if (Tokeniser.hasMoreTokens() & tokenConsumed)
                Tokeniser.advance();
            else if (tokenConsumed)
                exitWithError("Unexpected end of input file reached");

            tokenConsumed = false;

        }

        private void consumeToken()
        {
            tokenConsumed = true;
        }

        private void writeToken()
        {
            token token = Tokeniser.tokenType();

            switch (token)
            {
                case token.IDENTIFIER:
                    writeIdentifier(Tokeniser.indentifier()) ;
                    break;

                case token.SYMBOL:
                    writeSymbol(Tokeniser.symbol());
                    break;
            }

            tokenConsumed = true;
        }

        private bool checkForSymbol (char symbol, bool errorIfFalse)
        {
            if (Tokeniser.tokenType() == token.SYMBOL)
                if (Tokeniser.symbol() == symbol)
                    return true;
            if (errorIfFalse)
                exitWithError(symbol.ToString() + " expected") ;
            return false;
        }

        private bool checkForClassVar()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
            {
                string keyword = Tokeniser.keyWord();               
                if (keyword == "static")
                {
                    currentKind = kind.STATIC;
                    return true;
                }           
                else if (keyword == "field")
                {
                    currentKind = kind.FIELD;
                    return true;                   
                }      
            }
            currentKind = kind.NONE;
            return false;
        }

        private bool checkForVar()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
                if (Tokeniser.keyWord() == "var")
                {
                    currentKind = kind.VAR;
                    return true;
                }
            currentKind = kind.NONE;
            return false;
        }

        private bool checkForStatements()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
                if ((Tokeniser.keyWord() == "let") | (Tokeniser.keyWord() == "if") | (Tokeniser.keyWord() == "while") | (Tokeniser.keyWord() == "do") | (Tokeniser.keyWord() == "return"))
                    return true;
            return false;
        }

        private bool checkForSubroutineDec()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
            {
                string keyword = Tokeniser.keyWord();
                if ((keyword == "constructor") | (keyword == "function") | (keyword == "method"))
                    return true;
            }
            return false;
        }

        private void checkForReturnType()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
                if (Tokeniser.keyWord() == "void" )
                    return;
            checkForVarType(true);
            return;
        }

        private void checkForSubroutineName()
        {
            if (Tokeniser.tokenType() != token.IDENTIFIER)
                exitWithError("Subroutine name expected");
        }

        private int compileVarList(bool listRequired, bool paramList)
        {
            int vars = 0;
            advanceTokeniser();
            if (checkForVarType(listRequired))
            {
                vars++;
                //consume var type
                consumeToken();

                //var name
                advanceTokeniser();
                checkForVarName();
                addToSymbolTable(Tokeniser.indentifier(), currentType, currentKind);
                
                //Check for optional extra variable
                advanceTokeniser();
                while (checkForSymbol(',', false) == true)
                {
                    vars++;
                    // consume ,
                    consumeToken();

                    if (paramList)
                    {
                        //var type
                        advanceTokeniser();
                        checkForVarType(true);
                        consumeToken();
                    }

                    //var Name
                    advanceTokeniser();
                    checkForVarName();
                    addToSymbolTable(Tokeniser.indentifier(), currentType, currentKind);
                    advanceTokeniser();
                }

                if (!paramList)
                {
                    //consume ;
                    advanceTokeniser();
                    checkForSymbol(';', true);
                    consumeToken();
                }
                currentType = "";
            }
            return vars;
        }

        private bool checkForVarType(bool errorIfFalse)
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
            {
                string keyword = Tokeniser.keyWord();
                if ((keyword == "int") | (keyword == "char") | (keyword == "boolean"))
                {
                    currentType = keyword;
                    return true;
                }
                    
            }
            else if(Tokeniser.tokenType() == token.IDENTIFIER)
            {
                currentType = Tokeniser.indentifier();
                identifierAsType = true;
                return true;
            }
            if (errorIfFalse)
                exitWithError("Variable type declaration expected");

            currentType = "";
            return false;
        }

        private void checkForVarName ()
        {
            if (Tokeniser.tokenType() != token.IDENTIFIER)
                exitWithError("Variable name expected");
        }

        private bool checkForExpression(bool errorIfFalse)
        {
            if (checkForTerm())
                return true;
            if (errorIfFalse)
                exitWithError("Expression expected");
            return false;
        }

        private bool checkForTerm()
        {
            if (Tokeniser.tokenType() == token.INT_CONST)
                return true;
            if (Tokeniser.tokenType() == token.STRING_CONST)
                return true;
            if (checkForKeywordConst())
                return true;
            if (Tokeniser.tokenType() == token.IDENTIFIER)  //varName or subroutineName
                return true;
            if (checkForSymbol('(', false))
                return true;

            //check for unaryOp
            if (checkForSymbol('-', false) | checkForSymbol('~', false))
                return true;

            return false;
        }

        public bool checkForKeywordConst()
        {
            if (Tokeniser.tokenType() == token.KEYWORD)
            {
                string key = Tokeniser.keyWord();
                if ((key == "true") | (key == "false") | (key == "null") | (key == "this"))
                    return true;
            }
            return false;
        }

        private bool checkForElse()
        {
            if (Tokeniser.tokenType() != token.KEYWORD)
                return false;
            if (Tokeniser.keyWord() != "else")
                return false;
            return true;
        }

        private bool checkForOp()
        {
            if (Tokeniser.tokenType() == token.SYMBOL)
            {
                char symbol = Tokeniser.symbol();
                switch (symbol)
                {
                    case '+':
                    case '-':
                    case '*':
                    case '/':
                    case '&':
                    case '|':
                    case '<':
                    case '>':
                    case '=':
                        return true;
                }
            }
            return false;
        }

        private bool checkDifferentClass(string name)
        {
            return ! (symbolTable.IndexOf(name) == -1);
        }

        private string getClassName(string name)
        {
            if (symbolTable.IndexOf(name) == -1)
                exitWithError("Object not in symbol table");

            return symbolTable.TypeOf(name);
        }

        private void exitWithError(string errorMessage)
        {
            this.Dispose();
            System.Console.WriteLine("Error - " + errorMessage);
            System.Environment.Exit(0);
        }

        private void writeIdentifier(string identifier)
        {
            string modified = identifier;
            string kind = "", type = "", index = "";

            if (currentKind == Compiler.kind.NONE)
                currentKind = Compiler.kind.CLASS;  //Unidentified identifier assumed class until otherwise proven

            if (currentKind == Compiler.kind.SUBROUTINE)
                currentType = "";

            if (symbolTable.IndexOf(identifier) == -1)
            {
                //Class or subroutine
                if (currentType == "")
                {
                    kind = currentKind.ToString();
                    type = "";
                    index = "";
                }
                //Not a class or subroutine. Add to symbol table
                else if (!(identifierAsType))
                {
                    symbolTable.define(identifier, currentType, currentKind);
                    kind = currentKind.ToString();
                    type = currentType;
                    index = symbolTable.IndexOf(identifier).ToString();
                }
            }
            else
            {
                kind = symbolTable.KindOf(identifier).ToString();
                type = symbolTable.TypeOf(identifier);
                index = symbolTable.IndexOf(identifier).ToString();
            }

            if (identifierAsType)
            {
                //Identifier used as a type
                identifierAsType = false;
                kind = Compiler.kind.CLASS.ToString();
                type = "";
                index = "";
            }

            modified = modified + " " + kind;
            modified = modified + " " + type;
            modified = modified + " " + index;
            
        }

        private void writeSymbol(char symbol)
        {
            switch (symbol)
            {
                case '+':
                    writeArithmetic(command.ADD);
                    break;

                case '-':
                    writeArithmetic(command.SUB);
                    break;

                case '*':
                    writeCall("Math.multiply", 2);
                    break;

                case '/':
                    writeCall("Math.divide", 2);
                    break;

                case '&':
                    writeArithmetic(command.AND);
                    break;

                case '|':
                    writeArithmetic(command.OR);
                    break;

                case '<':
                    writeArithmetic(command.LT);
                    break;

                case '>':
                    writeArithmetic(command.GT);
                    break;

                case '=':
                    writeArithmetic(command.EQ);
                    break;
            }
        }

        private void addToSymbolTable(string name, string type, kind kind)
        {
            symbolTable.define(name, type, kind);
            consumeToken();
        }

        private segment kindToSegment(kind kind)
        {
            switch (kind)
            {
                case kind.ARG:
                    return segment.argument;
                case kind.FIELD:
                    return segment.@this;
                case kind.STATIC:
                    return segment.@static;
                case kind.VAR:
                    return segment.local;
                default:
                    throw new NotImplementedException();
            }
        }

        // Saves token to enable looking at following token
        private void saveToken ()
        {
            token token = Tokeniser.tokenType();

            switch (token)
            {
                case token.IDENTIFIER:
                    savedToken = Tokeniser.indentifier();
                    break;
            }

            tokenConsumed = true;
        }

        private void writeVar(string var, bool consumeToken)
        {
            int index = symbolTable.IndexOf(var);
            kind kind;
            if (index != -1)
            {
                kind = symbolTable.KindOf(var);
                switch (kind)
                {
                    case kind.STATIC:
                        writePush(segment.@static, index, consumeToken);
                        break;
                    case kind.FIELD:
                        writePush(segment.@this, index, consumeToken);
                        break;
                    case kind.ARG:
                        writePush(segment.argument, index, consumeToken);
                        break;
                    case kind.VAR:
                        writePush(segment.local, index, consumeToken);
                        break;
                }
            }
        }

        private void writePush(segment segment, int index, bool consume)
        {
            Writer.writePush(segment, index);
            if (consume)
                consumeToken();
        }

        private void writePop(segment segment, int index, bool consume)
        {
            Writer.writePop(segment, index);
            if (consume)
                consumeToken();
        }

        private void writeArithmetic(command command)
        {
            Writer.writeArithmetic(command);
        }

        private void writeCall(string name, int nArgs)
        {
            Writer.writeCall(name, nArgs);
        }

        private void writeFunction(string name, int nLocals)
        {
            Writer.writeFunction(name, nLocals);
        }

        private void writeReturn()
        {
            Writer.writeReturn();
            consumeToken();
        }
        #endregion


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                Tokeniser.Dispose();
                Writer.Dispose();
            }

            disposed = true;
        }


    }
}
