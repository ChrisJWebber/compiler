﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Compiler
{
    class xmlWriter : IDisposable
    {
        private bool disposed = false;
        private string prefixTabs =  "";

        private StreamWriter xmlFile;

        #region publicMethods
        public xmlWriter(string filePath)
        {
            this.xmlFile = createXMLFile(filePath);
        }

        public void writeKeyword(string keyword )
        {
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<keyword> " + keyword + " </keyword>");
        }

        public void writeSymbol(char symbol)
        {
            //Certain characters are used for 
            string modifiedSymbol = symbol.ToString();
            if (symbol == '<')
                modifiedSymbol = "&lt;";

            if (symbol == '>')
                modifiedSymbol = "&gt;";

            if (symbol == '\"')
                modifiedSymbol = "&quot;";

            if (symbol == '&')
                modifiedSymbol = "&amp;";

            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<symbol> " + modifiedSymbol + " </symbol>");
        }

        public void writeIntConst(int intConst)
        {
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<integerConstant> " + intConst + " </integerConstant>");
        }

        public void writeStringConst(string stringConst)
        {
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<stringConstant> " + stringConst + " </stringConstant>");
        }

        public void writeIdentifier (string indentifier)
        {
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<identifier> " + indentifier + " </identifier>");
        }

        public void writeOpenTag (string tag)
        {
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("<{0}>", tag);
            prefixTabs = prefixTabs + "  ";
        }

        public void writeCloseTag (string tag)
        {
            prefixTabs = prefixTabs.Remove(prefixTabs.Length - 2);
            xmlFile.Write(prefixTabs);
            xmlFile.WriteLine("</{0}>", tag); 
        }

        public void close()
        {
            xmlFile.Close();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                xmlFile.Dispose();
            }

            disposed = true;
        }

        #endregion

        #region privateMethods
        private StreamWriter createXMLFile(string filePath)
        {
            filePath = filePath.Replace(".jack", "") + "T.xml";    // directory paths do not contain .vm
            Console.WriteLine(filePath + " created");
            Console.WriteLine();
            return new StreamWriter(filePath);
        }


        #endregion

    }
}
