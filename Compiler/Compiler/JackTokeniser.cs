﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Compiler
{
    class JackTokeniser : IDisposable
    {
        bool disposed = false;

        private StreamReader JackCode;
        string currentToken = "";

        //All possible Keywords
        string[] keywords = {"class", "constructor", "function", "method",
                             "field", "static", "var", "int", "char", "boolean",
                             "void", "true", "false", "null", "this", "let",
                             "do", "if", "else", "while", "return"};

        //All possible symbols
        string[] symbols = {"{", "}", "(", ")", "[", "]", ".", ",",
                           ";", "+", "-", "*", "/", "&", "|", "<",
                           ">", "=", "~"};


        #region publicMethods
        public JackTokeniser (string filePath)
        {
            this.JackCode = new StreamReader(filePath);
        }

        //Are there any more tokens in the input?
        public bool hasMoreTokens ()
        {
            removeWhiteSpace();
            return !JackCode.EndOfStream;
        }

        //Reads the next token from the input.
        public void advance  ()
        {
            //clear current token
            currentToken = "";

            do
            {
                removeWhiteSpace();

                bool commentRemoved = false;

                //Check for symbols
                foreach (string symbol in symbols)
                {
                    if (symbol.StartsWith(peekNextChar()))
                    {
                        if (symbol != "/")  //Defintly not a comment. All symbols other than comments are a complete token
                        {
                            currentToken = (readNextChar().ToString());
                            return;
                        }
                        else
                        {
                            currentToken = readNextChar().ToString();
                            if (peekNextChar() == '/') //start of comment
                            {
                                currentToken = "";      //clear stored /
                                JackCode.ReadLine();    //Throw away the rest of this line
                                commentRemoved = true;
                                break;
                            }
                            else if (peekNextChar() == '*')
                            {
                                currentToken = "";
                                while(peekNextChar() == '*')
                                {
                                    JackCode.ReadLine();
                                    removeWhiteSpace();
                                }
                            }
                        }
                    }
                }

                //Check for string constant
                if (peekNextChar() == '\"')
                {
                    currentToken = currentToken + readNextChar().ToString(); //add "
                    while (peekNextChar() != '\"')
                    {
                        currentToken = currentToken + readNextChar().ToString(); //Read all chars in string and append to token
                    }
                    currentToken = currentToken + readNextChar().ToString(); //add final "
                    return;
                }

                //Check for int constant
                if (char.IsNumber(peekNextChar()))
                {
                    while (char.IsNumber(peekNextChar()))     //Loop while next char is a number
                    {
                        currentToken = currentToken + readNextChar().ToString(); //Read all chars in int and append to token
                    }
                    return;
                }

                //Check for identifier / keyword
                if ((peekNextChar() == '_') | (char.IsLetterOrDigit(peekNextChar())))
                {
                    while ((peekNextChar() == '_') | (char.IsLetterOrDigit(peekNextChar())))
                        currentToken = currentToken + readNextChar().ToString(); //Read all chars in identifier and append to token
                    return;
                }


                if (!commentRemoved)
                {
                    //No valid token found
                   // Console.WriteLine("Invalid token found. Char=: {0} token=: {1} ", JackCode.Read().ToString(), currentToken);
                    break;
                }
            } while (currentToken == "");

        }

        //Returns type of current token
        public token tokenType ()
        {
            if (currentToken == "")
                return token.INVALID;                

            foreach (string key in keywords)
            {
                if (currentToken.Equals(key))
                    return token.KEYWORD;
            }

            foreach (string symbol in symbols)
            {
                if (currentToken.Equals(symbol))
                    return token.SYMBOL;
            }

            if (int.TryParse(currentToken, out _))
                return token.INT_CONST;
            
            if (currentToken.StartsWith('\"'))
                return token.STRING_CONST;

            if ((currentToken.StartsWith('_')) | (char.IsLetter(currentToken.ToCharArray()[0])))
                return token.IDENTIFIER;

            return token.INVALID;
        }

        //Returns the keyword which is in the current token
        public string keyWord ()
        {
            foreach (string key in keywords)
            {
                if (currentToken.Contains(key))
                    return key;
            }

            return "";
        }

        //Returns the character which is the current token
        public char symbol ()
        {
            return (currentToken.ToCharArray())[0];
        }

        //Returns the indentifier which is the current token
        public string indentifier ()
        {
            return currentToken;
        }

        //Returns the integer value of the current token
        public int intVal ()
        {
            return Int32.Parse(currentToken);
        }

        //Returns the string value of the current token
        //without the two enclosing double quotes
        public string stringVal ()
        {
            return currentToken.Replace("\"", "");
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                JackCode.Dispose();
            }

            disposed = true;
        }

        #endregion

        private char readNextChar ()
        {
            int c = JackCode.Read();
            return validateChar(c);
        }

        private char peekNextChar()
        {
            int c = JackCode.Peek();
            return validateChar(c);
        }

        private char validateChar(int c)
        {
            if (c == -1)
                return '\0';
            return (char)c;
        }

        private void removeWhiteSpace()
        {
            //Remove new lines and white space and tabs
            while ((peekNextChar() == '\r') | (peekNextChar() == '\n') | (peekNextChar() == ' ') | (peekNextChar() == '\t'))
                readNextChar();    //Throw away char
        }
    }
}
