﻿using System;
using System.IO;

namespace Compiler
{
    enum token
    {
        KEYWORD,
        SYMBOL,
        IDENTIFIER,
        INT_CONST,
        STRING_CONST,
        INVALID
    }

    // What kind of variable. 
    enum kind
    {
        STATIC,
        FIELD,
        ARG,
        VAR,
        CLASS,
        SUBROUTINE,
        NONE
    }

    //VM memory segments
    enum segment
    {
        argument,
        local,
        @static,
        @this,
        that,
        pointer,
        temp,
        constant
    }

    //VM arithmetic-logical commands
    enum command
    {
        ADD,
        SUB,
        NEG,
        EQ,
        GT,
        LT,
        AND,
        OR,
        NOT
    }

    class Program
    {
        static void Main(string[] args)
        {
            string[] filePaths = getTargetPath();

            foreach (string filePath in filePaths)
            {
                Console.WriteLine();
                Console.WriteLine("Compilation of {0} started", filePath);
                CompilationEngine engine = new CompilationEngine(filePath, filePath);
            }
        }

        private static string[] getTargetPath()
        {
            Console.WriteLine("Enter the directory or file path of the jack file(s) you would like to compile: ");
            string path = Console.ReadLine();

            if (path.EndsWith(".jack"))
            {
                if (File.Exists(path) == false)
                {
                    Console.WriteLine("The file cannot be found");
                    System.Environment.Exit(0);
                }
                return new string[] { path };
            }
            else if (path.Contains(".")) //i.e. is a non .jack file
            {
                Console.WriteLine("Invalid file format. The file must be a .jack file");
                System.Environment.Exit(0);
            }
            else if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path, "*.jack");
                if (files.Length == 0)
                {
                    Console.WriteLine("No .jack files found in target directory");
                    System.Environment.Exit(0);
                }
                return files;
            }
            else
            {
                Console.WriteLine("Directory or file not found");
                System.Environment.Exit(0);
            }

            return new string[] { "" };
        }

    }
}
