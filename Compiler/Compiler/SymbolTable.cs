﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Compiler
{
    class SymbolTable
    {

        Hashtable classTable, subroutineTable;

        readonly struct symbolInfo
        {
            public symbolInfo (string Type, kind Kind, int Index)
            {
                type = Type;
                kind = Kind;
                index = Index;
            }

            public string type { get;}
            public kind kind { get;}
            public int index { get;}
        };

        int argCount, varCount, staticCount, fieldCount = 0;

        //creates new symbol table
        public SymbolTable ()
        {
            classTable = new Hashtable();
            subroutineTable = new Hashtable();
        }

        #region publicMethods
        //Starts a new subroutine scope (i.e., resets the subroutine's symbol table)
        public void startSubroutine()
        {
            subroutineTable.Clear();
            argCount = 0;
            varCount = 0;
        }

        //Defines a new identifier of the given name, type, and kind
        //and assigns it a running index. 
        //Static and FIELD indentifiers have a class scope,
        //while ARG and VAR identifiers have a subroutine scope
        public void define (string name, string type, kind kind)
        {
            try
            {
                switch (kind)
                {
                    case kind.ARG:
                        subroutineTable.Add(name, new symbolInfo(type, kind, argCount++));
                        break;

                    case kind.VAR:
                        subroutineTable.Add(name, new symbolInfo(type, kind, varCount++));
                        break;

                    case kind.STATIC:
                        classTable.Add(name, new symbolInfo(type, kind, staticCount++));
                        break;

                    case kind.FIELD:
                        classTable.Add(name, new symbolInfo(type, kind, fieldCount++));
                        break;
                }
            }
            catch
            {
                System.Console.WriteLine("A definition for {0} already exists", name);
                System.Environment.Exit(0);
            }  
        }

        //Returns the number of variables of the given kind 
        public int VarCount (kind kind)
        {
            switch (kind)
            {
                case kind.ARG:
                    return argCount;
                case kind.VAR:
                    return varCount;
                case kind.STATIC:
                    return staticCount;
                case kind.FIELD:
                    return fieldCount;
                default:
                    return 0;
            }
        }

        //Returns the kind of the named identifier in the current scope
        //If the identifier is unknown returns NONE.
        public kind KindOf (string name)
        {
            if (subroutineTable.ContainsKey(name))
                return ((symbolInfo)subroutineTable[name]).kind;
            if (classTable.ContainsKey(name))
                return ((symbolInfo)classTable[name]).kind;
            return kind.NONE;
        }

        //Returns the type of the named identifier
        public string TypeOf (string name)
        {
            if (subroutineTable.ContainsKey(name))
                return ((symbolInfo)subroutineTable[name]).type;
            if (classTable.ContainsKey(name))
                return ((symbolInfo)classTable[name]).type;
            return "";
        }

        //Returns the index assigned to the named identifier
        public int IndexOf (string name)
        {
            if (subroutineTable.ContainsKey(name))
                return ((symbolInfo)subroutineTable[name]).index;
            if (classTable.ContainsKey(name))
                return ((symbolInfo)classTable[name]).index;
            return -1;
        }

        #endregion

        #region privateMethods

        #endregion

    }
}
