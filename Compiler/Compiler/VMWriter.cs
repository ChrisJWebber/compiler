﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Compiler
{
    class VMWriter : IDisposable
    {
        private bool disposed = false;
        private StreamWriter vmFile;
        //Creates a new output .vm file and prepares it for writing
        public VMWriter (string filePath)
        {
            this.vmFile = createVMFile(filePath);
        }

        #region publicMethods

        //Writes a VM push command
        public void writePush (segment segment, int index)
        {
            vmFile.WriteLine("push {0} {1}", segment.ToString(), index.ToString());
        }

        //Writes a VM pop command
        public void writePop(segment segment, int index)
        {
            vmFile.WriteLine("pop {0} {1}", segment.ToString(), index.ToString());
        }

        //Writes a VM arithmetic-logical command
        public void writeArithmetic (command command)
        {
            switch (command)
            {
                case command.ADD:
                    vmFile.WriteLine("add");
                    break;
                case command.SUB:
                    vmFile.WriteLine("sub");
                    break;
                case command.NEG:
                    vmFile.WriteLine("neg");
                    break;
                case command.EQ:
                    vmFile.WriteLine("eq");
                    break;
                case command.GT:
                    vmFile.WriteLine("gt");
                    break;
                case command.LT:
                    vmFile.WriteLine("lt");
                    break;
                case command.AND:
                    vmFile.WriteLine("and");
                    break;
                case command.OR:
                    vmFile.WriteLine("or");
                    break;
                case command.NOT:
                    vmFile.WriteLine("not");
                    break;
            }
        }

        //Writes a VM label command
        public void WriteLabel (string label)
        {
            vmFile.WriteLine("label {0}", label);
        }

        //Writes a VM goto command
        public void WriteGoto (string label)
        {
            vmFile.WriteLine("goto {0}", label);
        }

        //Writes a VM if-goto command
        public void WriteIf (string label)
        {
            vmFile.WriteLine("if-goto {0}", label);
        }

        //Writes a VM call command
        public void writeCall (string name, int nArgs)
        {
            vmFile.WriteLine("call {0} {1}", name, nArgs.ToString());
        }

        //Writes a VM function command
        public void writeFunction (string name, int nLocals)
        {
            vmFile.WriteLine("function {0} {1}", name, nLocals);
        }

        //Writes a VM return command
        public void writeReturn ()
        {
            vmFile.WriteLine("return");
        }

        //Closes the output file
        public void close ()
        {
            vmFile.Close();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        protected virtual void Dispose (bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
                vmFile.Dispose();
            disposed = true;
        }

        #region privateMethods
        private StreamWriter createVMFile(string filePath)
        {
            filePath = filePath.Replace(".jack", "") + ".vm";    // directory paths do not contain .vm
            Console.WriteLine(filePath + " created");
            Console.WriteLine();
            return new StreamWriter(filePath);
        }
        #endregion



    }
}
